DROP MATERIALIZED VIEW IF EXISTS mstjupiter.mstmvw_borehole_clayfraction;
CREATE MATERIALIZED VIEW mstjupiter.mstmvw_borehole_clayfraction AS (
	WITH 
		cf AS (
			SELECT 
			ls.boreholeid, 
			ls.rocksymbol, 
			ls.top, 
			ls.bottom,
			CASE 
				WHEN lf.lf_max IS NULL
					THEN 1
				ELSE lf.lf_max
				END AS cf_max,
			CASE 
				WHEN lf.lf_min IS NULL
					THEN 0
				ELSE lf.lf_min
				END AS cf_min
			FROM jupiter.lithsamp ls
			LEFT JOIN 
				(	SELECT lf.rocksymbol, avg(lf.lerfraktion_min/100) AS lf_min, avg(lf.lerfraktion_max/100) AS lf_max
					FROM mstjupiter.lerfraktion lf
					GROUP BY lf.rocksymbol
				) AS lf ON ls.rocksymbol = lf.rocksymbol
		),
		cf_0_5 AS (	
			SELECT
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 0
						THEN 0
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 5
						THEN 5
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 5  AND cf.bottom > 0 
		),
		cf_5_10 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 5
						THEN 5
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 10
						THEN 10
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 10  AND cf.bottom > 5 
		),
		cf_10_15 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 10
						THEN 10
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 15
						THEN 15
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 15  AND cf.bottom > 10 
		),
		cf_15_20 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 15
						THEN 15
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 20
						THEN 20
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 20  AND cf.bottom > 15
		),
		cf_20_25 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 20
						THEN 20
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 25
						THEN 25
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 25  AND cf.bottom > 20
		),
		cf_25_30 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 25
						THEN 25
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 30
						THEN 30
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 30  AND cf.bottom > 25
		),
		cf_30_40 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 30
						THEN 30
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 40
						THEN 40
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 40  AND cf.bottom > 30
		),
		cf_40_50 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 40
						THEN 40
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 50
						THEN 50
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 50  AND cf.bottom > 40
		),
		cf_50_60 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 50
						THEN 50
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 60
						THEN 60
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 60  AND cf.bottom > 50
		),
		cf_60_70 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 60
						THEN 60
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 70
						THEN 70
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 70  AND cf.bottom > 60
		),
		cf_70_80 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 70
						THEN 70
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 80
						THEN 80
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 80  AND cf.bottom > 70
		),
		cf_80_90 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 80
						THEN 80
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 90
						THEN 90
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 90  AND cf.bottom > 80
		),
		cf_90_100 AS (	
			SELECT 
				cf.boreholeid, cf.cf_min, cf.cf_max, cf.rocksymbol,
				CASE
					WHEN cf.top < 90
						THEN 90
					ELSE cf.top
					END AS top_edit,
				CASE
					WHEN cf.bottom > 100
						THEN 100
					ELSE cf.bottom
					END AS bottom_edit
			FROM cf
			WHERE cf.top < 100  AND cf.bottom > 90
		),
		combined AS (
			SELECT
				1 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max,
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_0_5
			GROUP BY boreholeid
			UNION 
			SELECT
				2 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_5_10
			GROUP BY boreholeid
			UNION 
			SELECT
				3 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_10_15
			GROUP BY boreholeid
			UNION 
			SELECT
				4 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_15_20
			GROUP BY boreholeid
			UNION 
			SELECT
				5 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_20_25
			GROUP BY boreholeid
			UNION 
			SELECT
				5 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_25_30
			GROUP BY boreholeid
			UNION 
			SELECT
				6 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_30_40
			GROUP BY boreholeid
			UNION 
			SELECT
				7 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_40_50
			GROUP BY boreholeid	
			UNION 
			SELECT
				8 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_50_60
			GROUP BY boreholeid
			UNION 
			SELECT
				9 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_60_70
			GROUP BY boreholeid	
			UNION 
			SELECT
				10 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_70_80
			GROUP BY boreholeid
			UNION 
			SELECT
				11 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_80_90
			GROUP BY boreholeid	
			UNION 
			SELECT
				12 AS intervalno, string_agg(rocksymbol, ', ') AS rocksymbols,
				boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
				sum((bottom_edit-top_edit)*cf_min) AS cl_min, 
				sum((bottom_edit-top_edit)*cf_max) AS cl_max,
				sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)) AS cf_min,
				sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)) AS cf_max
			FROM cf_90_100
			GROUP BY boreholeid		
		)
	SELECT
		ROW_NUMBER() over () AS row_num,
		c.intervalno,
		b.boreholeno, 
		c.boreholeid,
		(c.bottom_max + c.top_min)/2 AS depth_point_m,
		(c.cf_min + c.cf_max)/2 AS cf_avg_frac,
		c.top_min_m AS top,
		c.bottom_max_m AS bottom,
		c.cf_min_frac,
		c.cf_max_frac,
		(c.cl_min + c.cf_max)/2 AS cl_avg_m,
		rocksymbols,
		b.geom,
		b.xutm32euref89,
		b.yutm32euref89
	FROM combined c
	INNER JOIN jupiter.borehole b ON c.boreholeid = b.boreholeid
	WHERE b.geom IS NOT NULL
		--AND b.boreholeno = '  1.   18'
		--AND b.boreholeno = ' 46.  734'
	ORDER BY boreholeid DESC, top_min
);

DROP MATERIALIZED VIEW IF EXISTS mstjupiter.mstmvw_borehole_clayfraction_test;
CREATE MATERIALIZED VIEW mstjupiter.mstmvw_borehole_clayfraction_test AS (
	SELECT row_num, cl_avg_m, geom
	FROM mstjupiter.mstmvw_borehole_clayfraction
	WHERE intervalno = 1
		and geom IS NOT NULL
);