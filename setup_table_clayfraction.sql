DROP TABLE IF EXISTS mstjupiter.lerfraktion;

CREATE TABLE IF NOT EXISTS mstjupiter.lerfraktion (
	beskrivelse	varchar(250),
	lerfraktion_avg float,
	lerfraktion_std float,
	lerfraktion_min float,
	lerfraktion_max float,
	number_of_samples int,
	kilde varchar(250)
);

INSERT INTO mstjupiter.lerfraktion 
	(beskrivelse, lerfraktion_avg, lerfraktion_std, lerfraktion_min, lerfraktion_max, number_of_samples, kilde)
VALUES 
	('Marint sand', 0.6, 0.78, 0, 2, 33, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Marint sand med silt', 8, 3.6, 0, 14, 41, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Marint finfraktion, sandet', 9, 3.4, 2, 12, 42, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Marint finfraktion, leret', 0.6, 0.78, 0, 2, 65, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Marint finfraktion, stærkt leret', 0.6, 0.78, 0, 2, 51, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Fluvialt sand med 0-5% ler+silt, 0-10% grus', 0.1, 0.39, 0, 3, 101, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Fluvialt sand med 0-3% ler og 6%-38% silt, 0-10% grus', 1, 1.29, 0, 3, 68, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Fluvialt sand med 4-11 % ler og 4%-40% silt, 0-10% grus', 6, 1.76, 4, 11, 30, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Fluvialt sand med 0-5% ler+silt, 11 %-38% grus', null, null, 0, 2, 91, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Fluvialt sand med 0-8% ler og 4%-20% silt, 12%-40% grus', null, null, 0, 8, 52, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Lakustrint silt', 11, 5.51, 0, 25, 32, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Lakustrint ler, senglaciale-glaciale landskab', 23, 7, 9, 40, 48, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Lakustrint ler, Baltiske issø', 71, 26.3, 21, 96, 12, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Morænesand', 9, 3.6, 0, 18, 154, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Morænesilt', 12, 4, 1, 19, 19, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Moræneler', 17, 4.1, 7, 34, 587, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Moræneler med højt lerindhold', 44, 5.2, 36, 52, 12, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Uspecificeret ler', 17, 6.2, 8, 35, 52, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('A-horisonter i morænesand', 6, 2.4, 2, 10, 16, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('A-horisonter i moræneler', 12, 4.6, 4, 18, 21, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('A-horisonter i senglacialt ferskvands sand', 4, 2.1, 0, 6, 7, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('A-horisonter i senglacialt ferskvandsler', 9, 2.8, 7, 11, 2, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('B-horisonter i moræneler', 22, 3.9, 13, 30, 22, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('Sand', null, NULL, 0, 3, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SAND med silt', null, NULL, 0, 3, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SAND, svagt leret', null, NULL, 3, 8, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SAND, leret', null, NULL, 8, 14, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SILT', null, NULL, 0, 8, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SILT, svagt leret', null, NULL, 0, 8, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SILT, leret', null, NULL, 8, 20, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('SILT, svagt leret', null, NULL, 0, 8, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER ', null, NULL, 65, 100, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER, siltet', null, NULL, 45, 65, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER, stærkt siltet', null, NULL, 20, 45, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER, moderat sandet', null, NULL, 25, 45, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER, sandet', null, NULL, 14, 25, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER, stærkt sandet', null, NULL, 14, 45, null, 'Forbian, A. ”Kornfordelingen i nogle danske jordarter.” (2000). Dansk geologisk forening for Geologisk Tidsskrift, hæfte 3, side 1-32.'),
	('LER, meget fedt', null, NULL, 80, 100, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('LER, fedt', null, NULL, 50, 80, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('LER, ret fedt', null, NULL, 30, 50, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('LER, siltet sandet', null, NULL, 0, 30, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('LER, stærkt siltet/sandet', null, NULL, 0, 30, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('SILT, strækt leret', null, NULL, 4, 7, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('SILT, leret/sandet', null, NULL, 0, 4, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNEGRUS', null, NULL, 0, 12, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNESAND', null, NULL, 0, 12, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNESILT', null, NULL, 0, 12, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNELER, st. siltet', null, NULL, 12, 15, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNELER, st. sandet', null, NULL, 12, 15, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNELER, sandet', null, NULL, 15, 20, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNELER, ret fedt', null, NULL, 20, 100, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.'),
	('MORÆNELER, fedt', null, NULL, 30, 100, null, 'Larsen, G., et al. "Vejledning i Ingeniørgeologisk prøvebeskrivelse." (1988). Dansk geoteknisk forening, bulletin 1.')
;

ALTER TABLE mstjupiter.lerfraktion
	ADD lerfraktion_midtpunkt_calc float;

UPDATE mstjupiter.lerfraktion
SET lerfraktion_midtpunkt_calc  = (lerfraktion_max + lerfraktion_min) / 2
WHERE beskrivelse = beskrivelse;

ALTER TABLE mstjupiter.lerfraktion
	ADD rocksymbol varchar(250);