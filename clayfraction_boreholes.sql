WITH 
	cf AS (
		SELECT 
		ls.boreholeid, 
		ls.rocksymbol, 
		ls.top, 
		ls.bottom,
		CASE 
			WHEN rocksymbol = 'ds'
				THEN 0.11
			WHEN rocksymbol = 'ml'
				THEN 0.34
			ELSE 1
			END AS cf_max,
		CASE 
			WHEN rocksymbol = 'ds'
				THEN 0
			WHEN rocksymbol = 'ml'
				THEN 0.07
			ELSE 0
			END AS cf_min
		FROM jupiter.lithsamp ls
	),
	cf_0_5 AS (	
		SELECT
			cf.boreholeid, cf.cf_min, cf.cf_max,
			CASE
				WHEN cf.top < 0
					THEN 0
				ELSE cf.top
				END AS top_edit,
			CASE
				WHEN cf.bottom > 5
					THEN 5
				ELSE cf.bottom
				END AS bottom_edit
		FROM cf
		WHERE cf.top < 5  AND cf.bottom > 0 
	),
	cf_5_10 AS (	
		SELECT 
			cf.boreholeid, cf.cf_min, cf.cf_max,
			CASE
				WHEN cf.top < 5
					THEN 5
				ELSE cf.top
				END AS top_edit,
			CASE
				WHEN cf.bottom > 10
					THEN 10
				ELSE cf.bottom
				END AS bottom_edit
		FROM cf
		WHERE cf.top < 10  AND cf.bottom > 5 
	),
	cf_10_15 AS (	
		SELECT 
			cf.boreholeid, cf.cf_min, cf.cf_max,
			CASE
				WHEN cf.top < 10
					THEN 10
				ELSE cf.top
				END AS top_edit,
			CASE
				WHEN cf.bottom > 15
					THEN 15
				ELSE cf.bottom
				END AS bottom_edit
		FROM cf
		WHERE cf.top < 15  AND cf.bottom > 10 
	),
	cf_15_20 AS (	
		SELECT 
			cf.boreholeid, cf.cf_min, cf.cf_max,
			CASE
				WHEN cf.top < 15
					THEN 15
				ELSE cf.top
				END AS top_edit,
			CASE
				WHEN cf.bottom > 20
					THEN 20
				ELSE cf.bottom
				END AS bottom_edit
		FROM cf
		WHERE cf.top < 20  AND cf.bottom > 15
	),
	combined AS (
		SELECT
			1 AS intervalno,
			boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
			round(sum((bottom_edit-top_edit)*cf_min),2) AS cl_min, 
			round(sum((bottom_edit-top_edit)*cf_max),2) AS cl_max,
			round(sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)),2) AS cf_min,
			round(sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)),2) AS cf_max
		FROM cf_0_5
		GROUP BY boreholeid
		UNION 
		SELECT
			2 AS intervalno,
			boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
			round(sum((bottom_edit-top_edit)*cf_min),2) AS cl_min, 
			round(sum((bottom_edit-top_edit)*cf_max),2) AS cl_max,
			round(sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)),2) AS cf_min,
			round(sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)),2) AS cf_max
		FROM cf_5_10
		GROUP BY boreholeid
		UNION 
		SELECT
			3 AS intervalno,
			boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
			round(sum((bottom_edit-top_edit)*cf_min), 2) AS cl_min, round(sum((bottom_edit-top_edit)*cf_max), 2) AS cl_max,
			round(sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)),2) AS cf_min,
			round(sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)),2) AS cf_max
		FROM cf_10_15
		GROUP BY boreholeid
		UNION 
		SELECT
			4 AS intervalno,
			boreholeid, min(top_edit) AS top_min, max(bottom_edit) AS bottom_max, 
			round(sum((bottom_edit-top_edit)*cf_min),2) AS cl_min, 
			round(sum((bottom_edit-top_edit)*cf_max),2) AS cl_max,
			round(sum((bottom_edit-top_edit)*cf_min)/(max(bottom_edit)-min(top_edit)),2) AS cf_min,
			round(sum((bottom_edit-top_edit)*cf_max)/(max(bottom_edit)-min(top_edit)),2) AS cf_max
		FROM cf_15_20
		GROUP BY boreholeid
	)
SELECT
	ROW_NUMBER() over () AS row_num,
	b.boreholeno, 
	combined.*, 
	b.geom 
FROM combined
INNER JOIN jupiter.borehole b ON combined.boreholeid = b.boreholeid
--WHERE b.geom IS NOT NULL
	--AND b.boreholeno = '  1.   18'
	--AND b.boreholeno = ' 46.  734'
ORDER BY boreholeid DESC, top_min